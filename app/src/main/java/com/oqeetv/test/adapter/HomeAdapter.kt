package com.oqeetv.test.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.oqeetv.test.R
import com.oqeetv.test.data.SearchResult
import com.oqeetv.test.viewholder.MovieViewHolder

class HomeAdapter(private val onClickListener: OnItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = mutableListOf<SearchResult>()

    fun setItems(resultList: List<SearchResult>) {
        val result = DiffUtil.calculateDiff(DiffCallback(items, resultList))
        items.clear()
        items.addAll(resultList)
        result.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_item, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MovieViewHolder -> {
                holder.bind(items[position])
                holder.itemView.setOnClickListener {
                    onClickListener.onItemClick(items[position], holder.itemView, holder.posterImageView)
                }
            }
        }
    }

    private class DiffCallback(
        private val oldItems: List<SearchResult>,
        private val newItems: List<SearchResult>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldItems.size

        override fun getNewListSize() = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition].page.title == newItems[newItemPosition].page.title

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition] == newItems[newItemPosition]

    }

    interface OnItemClickListener {
        fun onItemClick(item: SearchResult, view: View, imageView: ImageView)
    }
}
