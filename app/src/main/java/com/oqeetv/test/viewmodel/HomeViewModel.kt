package com.oqeetv.test.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oqeetv.test.api.OpeeApi
import com.oqeetv.test.data.SearchResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(private val opeeApi: OpeeApi) : ViewModel() {

    data class Model(
        val resultList: List<SearchResult>
    )

    private val _model = MutableLiveData<Model>()
    val model: LiveData<Model> = _model

    fun getAll() {
        viewModelScope.launch(Dispatchers.IO) {
            _model.postValue(
                Model(
                    opeeApi.getAll()
                )
            )
        }
    }
}
