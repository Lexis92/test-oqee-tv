package com.oqeetv.test.viewholder

import android.view.View
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.oqeetv.test.R
import com.oqeetv.test.data.SearchResult
import kotlinx.android.synthetic.main.view_list_item.view.*

class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var posterImageView: ImageView = itemView.findViewById(R.id.list_item_img)

    fun bind(searchResult: SearchResult) {
        ViewCompat.setTransitionName(posterImageView, searchResult.page.title)
        Glide.with(itemView.context)
            .load(searchResult.clips[0].thumbnail)
            .into(posterImageView)

        itemView.list_item_title.text = searchResult.page.title
    }
}
