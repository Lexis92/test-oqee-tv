package com.oqeetv.test.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchResult(
    @SerializedName("page") val page: Page,
    @SerializedName("clips") val clips: List<Clips>
) : Parcelable

@Parcelize
data class Page(
    @SerializedName("movie_title") val title: String
) : Parcelable

@Parcelize
data class Clips(
    @SerializedName("thumb") val thumbnail: String,
    @SerializedName("versions") val versions: Versions
) : Parcelable

@Parcelize
data class Versions(
    @SerializedName("enus") val enus: Enus
) : Parcelable

@Parcelize
data class Enus(
    @SerializedName("sizes") val sizes: Sizes
) : Parcelable

@Parcelize
data class Sizes(
    @SerializedName("sd") val sd: Trailer
) : Parcelable

@Parcelize
data class Trailer(
    @SerializedName("srcAlt") val src: String
) : Parcelable
