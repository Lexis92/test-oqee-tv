package com.oqeetv.test.api

import com.oqeetv.test.data.SearchResult
import retrofit2.Call
import retrofit2.http.GET

interface OpeeApiService {

    @GET("movies.json")
    fun getAll(): Call<List<SearchResult>>
}
