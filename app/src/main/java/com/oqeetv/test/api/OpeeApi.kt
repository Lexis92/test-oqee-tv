package com.oqeetv.test.api

import com.oqeetv.test.data.SearchResult

class OpeeApi(private val opeeApiService: OpeeApiService) {

    fun getAll(): List<SearchResult> =
        opeeApiService.getAll()
            .execute()
            .body()
            ?: emptyList()
}
