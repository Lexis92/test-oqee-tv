package com.oqeetv.test.api;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Arrays;
import java.util.Collection;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okio.Buffer;

public class UnsafeOkHttpClient {
    public static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    trustManagerForCertificates(trustedCertificatesInputStream())
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Returns an input stream containing one or more certificate PEM files. This implementation just
     * embeds the PEM files in Java strings; most applications will instead read this from a resource
     * file that gets bundled with the application.
     */
    private static InputStream trustedCertificatesInputStream() {
        // PEM files for root certificates of Comodo and Entrust. These two CAs are sufficient to view
        // https://publicobject.com (Comodo) and https://squareup.com (Entrust). But they aren't
        // sufficient to connect to most HTTPS sites including https://godaddy.com and https://visa.com.
        // Typically developers will need to get a PEM file from their organization's TLS administrator.
        String comodoRsaCertificationAuthority =
                "-----BEGIN CERTIFICATE-----\n" +
                "MIIDbzCCAlegAwIBAgIUTHilMcrZees/uRY+LuVa+2MegrAwDQYJKoZIhvcNAQEL\n" +
                "BQAwRzELMAkGA1UEBhMCRlIxFzAVBgNVBAgMDlNvbWV3aGVyZSBuaWNlMQ0wCwYD\n" +
                "VQQKDARUUkFYMRAwDgYDVQQDDAdUcmF4IENBMB4XDTE5MDYxMzIzMzY1OFoXDTI5\n" +
                "MDYxMDIzMzY1OFowRzELMAkGA1UEBhMCRlIxFzAVBgNVBAgMDlNvbWV3aGVyZSBu\n" +
                "aWNlMQ0wCwYDVQQKDARUUkFYMRAwDgYDVQQDDAdUcmF4IENBMIIBIjANBgkqhkiG\n" +
                "9w0BAQEFAAOCAQ8AMIIBCgKCAQEAookji0vL8/+ok3J/pj59ckGYSHxDWzlDCaP4\n" +
                "Qo18f4TINzbqmIguOZxneicEKV7A+goPJJZSVsFwF58SckmHX4bK1yDoio/bUnSl\n" +
                "TD89M9GmvXs7EaTPSwW9vo21Dn31yrM1ZvFSNoca+RNCJj0/AODYN96TCZSYBWIv\n" +
                "o54XEUuxxVzaguHfqLuCFGlUxgVgzVaQICJXWaXRf+sSW3xtp2S7/Uo6DAaRDngJ\n" +
                "4h+bgvg357fQqIA291k1WbabCmpobrpWMmWA6lHh1Wss7yUfeoO26yr8qu6/tEpu\n" +
                "xbCnIoUHDO1C6y87v9nP4A29PCf69vK+lX+Ck94+T7udNCSkawIDAQABo1MwUTAd\n" +
                "BgNVHQ4EFgQUuhC6ZP0sImTHV1l5jtPQ6JbPpkUwHwYDVR0jBBgwFoAUuhC6ZP0s\n" +
                "ImTHV1l5jtPQ6JbPpkUwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOC\n" +
                "AQEAKtpOcMF+nXzWm1r9GXxLGfLw04oHtFnHgXPjv/62LRxYacI/z4dVJ0sDBDjl\n" +
                "ZWZx5UqrAObsWdPOBygE6JHp2RaOe/Ai/34FkKj7UYu75teuEasfnwW/AyPgiYlc\n" +
                "yHEmIcI0IjCJKzFlA3HKCG+crc02JggLAnHWenDYKgFsbcHZzRaANPCSkSzeuG90\n" +
                "091rHKpqqjASNtq/6w1B/zecwY8DcNs7X94FTqDKuKIwykByz7aADB4N2Gbd6EAK\n" +
                "l8RKV8JdbDdBZ++REng6YMrwvAkKkqMEnLy+5pcxeXQDHC0pciz+/+0DlBdCjT/Z\n" +
                "WUBYiZg0IgbbV8SqxOaQYK6lhw==\n" +
                "-----END CERTIFICATE-----\n";
        return new Buffer()
                .writeUtf8(comodoRsaCertificationAuthority)
                .inputStream();
    }

    /**
     * Returns a trust manager that trusts {@code certificates} and none other. HTTPS services whose
     * certificates have not been signed by these certificates will fail with a {@code
     * SSLHandshakeException}.
     *
     * <p>This can be used to replace the host platform's built-in trusted certificates with a custom
     * set. This is useful in development where certificate authority-trusted certificates aren't
     * available. Or in production, to avoid reliance on third-party certificate authorities.
     *
     * <p>See also {@link okhttp3.CertificatePinner}, which can limit trusted certificates while still using
     * the host platform's built-in trust store.
     *
     * <h3>Warning: Customizing Trusted Certificates is Dangerous!</h3>
     *
     * <p>Relying on your own trusted certificates limits your server team's ability to update their
     * TLS certificates. By installing a specific set of trusted certificates, you take on additional
     * operational complexity and limit your ability to migrate between certificate authorities. Do
     * not use custom trusted certificates in production without the blessing of your server's TLS
     * administrator.
     */
    private static X509TrustManager trustManagerForCertificates(InputStream in)
            throws GeneralSecurityException {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        Collection<? extends Certificate> certificates = certificateFactory.generateCertificates(in);
        if (certificates.isEmpty()) {
            throw new IllegalArgumentException("expected non-empty set of trusted certificates");
        }

        // Put the certificates a key store.
        char[] password = "password".toCharArray(); // Any password will work.
        KeyStore keyStore = newEmptyKeyStore(password);
        int index = 0;
        for (Certificate certificate : certificates) {
            String certificateAlias = Integer.toString(index++);
            keyStore.setCertificateEntry(certificateAlias, certificate);
        }

        // Use it to build an X509 trust manager.
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(
                KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, password);
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
            throw new IllegalStateException("Unexpected default trust managers:"
                    + Arrays.toString(trustManagers));
        }
        return (X509TrustManager) trustManagers[0];
    }

    private static KeyStore newEmptyKeyStore(char[] password) throws GeneralSecurityException {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream in = null; // By convention, 'null' creates an empty key store.
            keyStore.load(in, password);
            return keyStore;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
