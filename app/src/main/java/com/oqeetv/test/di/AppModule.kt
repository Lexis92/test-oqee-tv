package com.oqeetv.test.di

import com.oqeetv.test.api.OpeeApi
import com.oqeetv.test.api.OpeeApiService
import com.oqeetv.test.api.UnsafeOkHttpClient
import com.oqeetv.test.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient


val appModule = module {

    single {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://testepg.r0ro.fr/")
            .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        OpeeApi(retrofit.create(OpeeApiService::class.java))
    }

    viewModel {
        HomeViewModel(get())
    }
}
