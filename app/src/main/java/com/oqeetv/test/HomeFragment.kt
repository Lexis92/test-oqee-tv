package com.oqeetv.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.oqeetv.test.adapter.HomeAdapter
import com.oqeetv.test.data.SearchResult
import com.oqeetv.test.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject

class HomeFragment : Fragment() {

    private val viewModel by inject<HomeViewModel>()

    private val homeAdapter by lazy {
        HomeAdapter(object : HomeAdapter.OnItemClickListener {

            override fun onItemClick(item: SearchResult, view: View, imageView: ImageView) {
                val extras = FragmentNavigatorExtras(
                    imageView to (ViewCompat.getTransitionName(imageView) ?: "")
                )
                val directions = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(item)
                findNavController().navigate(directions, extras)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_home, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        postponeEnterTransition()
        view.doOnPreDraw { startPostponedEnterTransition() }

        home_recycler.adapter = homeAdapter

        viewModel.model.observe(this, Observer {
            homeAdapter.setItems(it.resultList)
        })
        // TODO do this once
        viewModel.getAll()
    }
}
